model = Backbone.View.extend({
    initialize: function() {
        this.render();
    },
    render: function() {
        var template = _.template($("#myScript").html(), {});
        this.$el.html(template);
    },
    events: {
        "mouseover span.hai": "mousedover",
        "click": "clicked"
    },
    clicked: function() {
        alert("clicked.");
    },
    mousedover: function() {
        alert("mouseover, it turn me on");
    }
});

$(document).ready(function() {
    var new1 = new model({ el: $("#container") });
});