model = Backbone.Model.extend({
    default: {
        color: "yellow"
    },
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        this.on("change", function(model) {
            var my_color = model.get("color");
            alert("change my pencil color to " + my_color);
        });
    }
});

$(document).ready(function() {
    var new1 = new model();
    new1.set({ color: "blue" });
});