StudentModel = Backbone.Model.extend({
    default: {
        name: "unknown"
    }
});

StudentCollection = Backbone.Collection.extend({
    model: StudentModel,
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        this.on("change:name", function(model) {
            alert("Hey, the name changed " + model.get("name"));
        });
        this.on("add", function(model) {
            alert("we just added model " + model.get("name"));
        });
        this.on("remove", function(model) {
            alert("omg we lost model " + model.get("name"));
        });
    }
});

$(document).ready(function() {
    var model1 = new StudentModel({ name: "agung", id: 0 });
    var model2 = new StudentModel({ name: "handaru", id: 1 });
    var class_1 = new StudentCollection([model1]);
    class_1.add([model2]);
    displayCollection("model ", class_1);
    class_1.remove([model2]);
    model1.set({ name: "gung" });
    displayCollection("model ", class_1);

});

function displayCollection(string, collection) {
    console.log(string + " " + JSON.stringify(collection.toJSON()));
}