StudentModel = Backbone.Model.extend({
    default: {
        name: "unknown"
    }
});

StudentCollection = Backbone.Collection.extend({
    model: StudentModel
});

$(document).ready(function() {
    var model1 = new StudentModel({ name: "agung", id: 0 });
    var model2 = new StudentModel({ name: "handaru", id: 1 });
    var class_1 = new StudentCollection([model1, model2]);
    console.log("Class one size : " + class_1.size());
    displayCollection("Two elements :", class_1);

    var model3 = new StudentModel({ name: "jati", id: 2 });
    class_1.add([model3]);
    displayCollection("Thre elements : ", class_1);
    class_1.remove([model3]);
    displayCollection("Two elemetns : ", class_1);

    var getModel = class_1.get(0);
    getModel.set({ name: "agung2" });
    displayCollection("Change name agung", getModel);
    model1.set({ name: "agung3" });
    displayCollection("Change name agung", getModel);
});

function displayCollection(string, collection) {
    console.log(string + " " + JSON.stringify(collection.toJSON()));
}