model = Backbone.View.extend({
    initialize: function() {
        this.render();
    },
    render: function() {
        var template = _.template($("#myScript").html(), {});
        this.$el.html(template);
    }
});

$(document).ready(function() {
    var new1 = new model({ el: $("#container") });
});